package cz.applifting.a11yInCompose

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.selection.toggleable
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Delete
import androidx.compose.material.icons.filled.Edit
import androidx.compose.material3.Checkbox
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.semantics.CollectionInfo
import androidx.compose.ui.semantics.CollectionItemInfo
import androidx.compose.ui.semantics.CustomAccessibilityAction
import androidx.compose.ui.semantics.clearAndSetSemantics
import androidx.compose.ui.semantics.collectionInfo
import androidx.compose.ui.semantics.collectionItemInfo
import androidx.compose.ui.semantics.customActions
import androidx.compose.ui.semantics.heading
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.semantics.stateDescription
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import kotlin.random.Random

@Preview(showBackground = true)
@Composable
private fun BetterScreenPreview() {
    A11yInComposeTheme {
        Surface {
            Column(
                modifier = Modifier.semantics {
                    collectionInfo = CollectionInfo(6, 1)
                },
            ) {
                Text(
                    text = "Top items",
                    style = MaterialTheme.typography.headlineSmall,
                    modifier = Modifier.semantics { heading() },
                )
                BetterItem(0)
                BetterItem(1)
                BetterItem(2)
                Text(
                    text = "Other items",
                    style = MaterialTheme.typography.headlineSmall,
                    modifier = Modifier.semantics { heading() },
                )
                BetterItem(3)
                BetterItem(4)
                BetterItem(5)
            }
        }
    }
}

@Composable
private fun BetterItem(index: Int) {
    var selected by remember { mutableStateOf(Random.nextBoolean()) }
    Row(
        verticalAlignment = Alignment.CenterVertically,
        modifier = Modifier
            // .semantics(mergeDescendants = true) { } // not needed - merge done also by toggleable
            .semantics {
                stateDescription = if (selected) "Selected" else "Not selected"
                customActions = listOf(
                    CustomAccessibilityAction("Edit") { true },
                    CustomAccessibilityAction("Delete") { true },
                )
                collectionItemInfo = CollectionItemInfo(index, 1, 0, 1)
            }
            .toggleable(value = selected, onValueChange = { selected = !selected }),
    ) {
        Spacer(modifier = Modifier.width(8.dp))
        Checkbox(
            checked = selected,
            onCheckedChange = { selected = !selected },
            modifier = Modifier.clearAndSetSemantics { },
        )
        Spacer(modifier = Modifier.width(8.dp))
        Column {
            Text(
                text = "This is me",
                style = MaterialTheme.typography.bodyLarge,
            )
            Text(
                text = "Yep, I'm a person",
                style = MaterialTheme.typography.bodyMedium,
            )
            Text(
                "I like ice-cream ❤️",
                style = MaterialTheme.typography.bodySmall,
                color = MaterialTheme.colorScheme.onSurfaceVariant,
            )
        }
        Spacer(modifier = Modifier.weight(1f))
        IconButton(onClick = { }, modifier = Modifier.clearAndSetSemantics { }) {
            Icon(
                imageVector = Icons.Default.Edit,
                contentDescription = null,
            )
        }
        IconButton(onClick = { }, modifier = Modifier.clearAndSetSemantics { }) {
            Icon(
                imageVector = Icons.Default.Delete,
                contentDescription = null,
            )
        }
        Spacer(modifier = Modifier.width(8.dp))
    }
}

package cz.applifting.a11yInCompose

import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.sp

@Preview(showBackground = true)
@Composable
private fun SmallestButtonPreview() {
    A11yInComposeTheme {
        Button(onClick = { }) {
            Text(text = "🤏", fontSize = 8.sp)
        }
    }
}
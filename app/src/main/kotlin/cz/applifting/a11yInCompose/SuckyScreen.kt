package cz.applifting.a11yInCompose

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Delete
import androidx.compose.material.icons.filled.Edit
import androidx.compose.material3.Checkbox
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.semantics.clearAndSetSemantics
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import kotlin.random.Random

@Preview(showBackground = true)
@Composable
private fun SuckyScreenPreview() {
    A11yInComposeTheme {
        Surface {
            Column {
                Text(text = "Top items", style = MaterialTheme.typography.headlineSmall)
                SuckyItem()
                SuckyItem()
                SuckyItem()
                Text(text = "Other items", style = MaterialTheme.typography.headlineSmall)
                SuckyItem()
                SuckyItem()
                SuckyItem()
            }
        }
    }
}

@Composable
private fun SuckyItem() {
    var selected by remember { mutableStateOf(Random.nextBoolean()) }
    Row(
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.spacedBy(16.dp),
        modifier = Modifier.padding(horizontal = 16.dp),
    ) {
        Checkbox(
            checked = selected,
            onCheckedChange = null,
            modifier = Modifier.clickable { selected = !selected },
        )
        Column {
            Text(
                text = "This is me",
                style = MaterialTheme.typography.bodyMedium,
            )
            Text(
                text = "Yep, I'm a person",
                style = MaterialTheme.typography.bodySmall,
                fontSize = 10.sp,
                modifier = Modifier.clearAndSetSemantics { },
            )
            Text(
                "I like ice-cream ❤️",
                style = MaterialTheme.typography.bodySmall,
                color = Color(0xFFBBBBBB),
            )
        }
        Spacer(modifier = Modifier.weight(1f))
        Icon(
            imageVector = Icons.Default.Edit,
            contentDescription = null,
            modifier = Modifier.clickable { }
        )
        Icon(
            imageVector = Icons.Default.Delete,
            contentDescription = null,
            modifier = Modifier.clickable { },
        )
    }
}
